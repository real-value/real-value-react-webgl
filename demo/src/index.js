import React, { useState } from 'react';
import { render} from 'react-dom';

import { SceneComponent, TryItComponent,EvaluateComponent } from '../../src';
// import debug from 'debug'
localStorage.debug = 'stream,scheduler'

// var log = debug('stream:log');
// log.log = console.log.bind(console); 

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

import { Tutorials,Nodes} from 'real-value-lang'

console.log(Tutorials)

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
        margin: 5,

        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
    example: {
        'background-color': '#EEE'
    }
  }));



const App = () => {

    const classes = useStyles()

    const [expression, setExpression] = useState("model.from('1,2,3,4').to('out.var')");

    const [viewMode,setViewMode] = useState('Tutorial')

    const switchView = (event) => {
        setViewMode(event)
    };

    switch(viewMode) {
        case 'Tutorial': 

        
        return <div className={classes}>
                <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <button onClick={()=>switchView('Docs')} >View Docs</button>
                        <button onClick={()=>switchView('Try')} >Try It</button>
                    </Paper>
                </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            Tutorial
                        </Paper>
                    </Grid>
                    {Tutorials.map((t)=><EvaluateComponent container content={t.d} logic={t.l} expression={t.x} multiresults={t.multiresults} enabled={t.enabled}/>
                                )}
                </Grid>
                </div>

        case 'Try': 
            return <div className={classes}>
                    <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <button onClick={()=>switchView('Docs')} >View Docs</button>
                            <button onClick={()=>switchView('Tutorial')} >View Tutorial</button>
                        </Paper>
                    </Grid>
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                Try It
                            </Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <TryItComponent expression={expression} setExpression={setExpression}/>
                                <SceneComponent  width="600" height="200" expression={expression} />
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
        case 'Docs' :
            const expresssions = [
                "model.from('').map().table().to('')",
                "model.from('').tap().filter().to('')",
                "let s = model.from(''); let s2 = model.from(''); s.merge(s2).tap()",
                "let s = model.from(''); let s2 = model.from(''); s.join(s2).tap()",
                "let s1 = model.from('');let s2 = s1;s1.tap();s2.map()"
            ]


                return <div className={classes}>
                    <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <button onClick={()=>switchView('Try')} >Try It</button>
                            <button onClick={()=>switchView('Tutorial')} >View Tutorial</button>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            Node Types
                        </Paper>
                    </Grid>

                    {Nodes.map((node)=>
                    <Grid item xs={3}>
                        <Paper className={classes.paper}>
                            <div>{node}</div>
                            <SceneComponent  width="200" height="200" showDescription={true} expression={node}/>  
                        </Paper>
                    </Grid>
                    )}

            
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            Expression Examples
                        </Paper>
                    </Grid> 
                    
                    {expresssions.map((expression)=>
                        <Grid item xs={6}>
                        <Paper className={classes.paper}>
                            <div>{expression}</div>
                            <SceneComponent  width="800" height="400" showDescription={false} expression={expression}/>  
                        </Paper>
                    </Grid>
                    )}
    
                </Grid>
            </div>
        }
        
    }

render(<App />, document.querySelector('#demo'));

