import { describe } from 'riteway';
import render from 'riteway/render-component';
import React from 'react';

import SceneComponent from '../src/scene-component';

describe('ClickComponent should render properly', async (assert) => {

  const createScene = value =>
    render(<SceneComponent aprop={ value } />)

    const value = 'unused';
    const $ = createScene(value);

    assert({
      given: 'a scene component',
      should: 'Should render the scene but has not ',
      actual: $('div')
        .html().trim(),
      expected: 'Webgl'
    });
});
