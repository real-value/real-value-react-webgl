# Value Flow React Webgl Component    

## About this component

This component renders real value flow expressions into webgl in order to visualize the data flow produced.


When you run *npm start* it runs an application which provides a visualization of stream nodes and some model examples.

## How set up

```
npm install
```

### How to use

Run the server
```
npm run start
```