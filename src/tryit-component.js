import React, { useEffect, useState } from 'react';

import { Model as modelFactory } from 'real-value-lang'


const TryItComponent =  (props) => {

  let {expression,setExpression} = props
  
  const [result, setResult] = useState('Expression Result');

  let variables = {}

  const handleChange = (event) => {
    setExpression(event.target.value);
  };

  const evalue = (event) => {
    let model = modelFactory({variables})
    //console.log(expression)
    try {
      eval(expression)
      model.run(()=>{
        let res = variables['out.var']
        console.log(res)
        setResult(res)
      })
    }catch(ex){
      console.log(ex)
      setResult(ex.toString())
    }
  };

  return <div>
          <textarea id='source' rows="10" cols="70" defaultValue={expression} onChange={handleChange}/>
          <button onClick={evalue}>Eval</button>
          <textarea rows="10" cols="70" readOnly value={result}/>
      </div>
}

export default TryItComponent
