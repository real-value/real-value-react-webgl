import React, { useEffect } from 'react';

import * as THREE from 'three'

import { TrackballControls } from './TrackballControls.js';

import { Model as modelFactory } from 'real-value-lang'
import  Topology from 'real-value-topology'

import uuidv4 from 'uuid/v4'

let NODE_SIZE=100
let HSZ=NODE_SIZE/2
let NOTCH_SIZE=10
let HN=NOTCH_SIZE/2

let sceneMax = 1

const SceneComponent =  (props) => {

  let sceneId = 'scene-'+ uuidv4()

  let {expression, showDescription} = props
  let model = modelFactory()
  let node = null
  console.log(expression)
  try {
    node = eval(expression)
  }catch(ex){
    console.log(ex)
  }
  
  function createShape(pts){
    return new THREE.Shape(pts.map((p)=>new THREE.Vector2(p[0],p[1])))
  }

  function getShape(type){
    var pts = [];
    if(type=='source'){
      return createShape([
        [-HSZ-10,HSZ-10],
        [-HSZ-10,HSZ-10],
        [-HSZ-10,HSZ-10],
        [HSZ-10,HSZ-10],
        [HSZ-10,NOTCH_SIZE],
        [HSZ-20,NOTCH_SIZE],
        [HSZ-20,-NOTCH_SIZE],
        [HSZ-10,-NOTCH_SIZE],
        [HSZ-10,-HSZ+10],
        [-HSZ-10,-HSZ+10]
      ])
    } else if(type=='sink'){
      return createShape([
        [-HSZ+10,HSZ-10],
        [HSZ-10,HSZ-10],
        [HSZ-10,-HSZ+10],
        [-HSZ+10,-HSZ+10],
        [-HSZ+10,-NOTCH_SIZE],
        [-HSZ+20,-NOTCH_SIZE],
        [-HSZ+20,NOTCH_SIZE],
        [-HSZ+10,NOTCH_SIZE]
      ])
    }else if(type=='map'){
      return createShape([
        [-HSZ+10,NOTCH_SIZE],
        [-HSZ+20,NOTCH_SIZE],
        [-HSZ+20,40],
        [0,NOTCH_SIZE],
        [HSZ-20,40],
        [HSZ-20,NOTCH_SIZE],
        [HSZ-10,NOTCH_SIZE],
        [HSZ-10,-NOTCH_SIZE],
        [HSZ-20,-NOTCH_SIZE],
        [HSZ-20,-40],
        [0,-NOTCH_SIZE],
        [-HSZ+20,-40],
        [-HSZ+20,-NOTCH_SIZE],
        [-HSZ+10,-NOTCH_SIZE]
      ])
    }else if(type=='tap' || type=='log'){
      return createShape([
        [-HSZ+10,NOTCH_SIZE],
        [-5,NOTCH_SIZE],
        [-5,20],
        [-10,20],
        [-10,30],
        [10,30],
        [10,20],
        [5,20],
        [5,NOTCH_SIZE],
        [10,NOTCH_SIZE],
        [30,NOTCH_SIZE],
        [30,-NOTCH_SIZE],
        [-40,-NOTCH_SIZE]
      ])
    }else if(type=='filter'){
      return createShape([
        [-HSZ+10,NOTCH_SIZE],
        [-HSZ+20,NOTCH_SIZE],
        [-HSZ+20,HSZ-10],
        [HSZ-20,NOTCH_SIZE],
        [HSZ-10,NOTCH_SIZE],
        [HSZ-10,-NOTCH_SIZE],
        [HSZ-20,-NOTCH_SIZE],
        [-HSZ+20,-HSZ+10],
        [-HSZ+20,-NOTCH_SIZE],
        [-HSZ+10,-NOTCH_SIZE]
      ])
    }else if(type=='connect'){
      return createShape([
        [-HSZ-NOTCH_SIZE,NOTCH_SIZE],
        [HSZ-20,NOTCH_SIZE],
        [HSZ-20,-NOTCH_SIZE],
        [-HSZ-NOTCH_SIZE,-NOTCH_SIZE]
      ])
    }else if(type=='pub'){
      return createShape([
        [-HSZ+NOTCH_SIZE,NOTCH_SIZE],
        [HN,NOTCH_SIZE],
        [HN,-HSZ],
        [-HN,-HSZ],
        [-HN,-NOTCH_SIZE],
        [-HSZ+NOTCH_SIZE,-NOTCH_SIZE]
      ])
    }else if(type=='merge'){
      return createShape([
        [-NOTCH_SIZE,HSZ-20],
        [-HSZ+NOTCH_SIZE,HSZ-20],
        [-HSZ+NOTCH_SIZE,HSZ-10],
        [0,HSZ-10],
        [0,HSZ-20],
        [0,NOTCH_SIZE],
        [HSZ-NOTCH_SIZE,NOTCH_SIZE],
        [HSZ-NOTCH_SIZE,-NOTCH_SIZE],
        [0,-NOTCH_SIZE],
        [0,-HSZ+NOTCH_SIZE],
        [-HSZ+NOTCH_SIZE,-HSZ+NOTCH_SIZE],
        [-HSZ+NOTCH_SIZE,-HSZ+20],
        [-NOTCH_SIZE,-HSZ+20]
        ])
    }else if(type=='endpoint'){
      return createShape([
          [0,0],
          [0,NOTCH_SIZE],
          [HSZ,NOTCH_SIZE],
          [HSZ,HSZ-2*NOTCH_SIZE],
          [HSZ+2*NOTCH_SIZE,HSZ-2*NOTCH_SIZE],
          [HSZ+2*NOTCH_SIZE,2*NOTCH_SIZE],
          [HSZ+NOTCH_SIZE,2*NOTCH_SIZE],
          [HSZ+NOTCH_SIZE,-NOTCH_SIZE],
          [HSZ+2*NOTCH_SIZE,-NOTCH_SIZE],
          [HSZ+2*NOTCH_SIZE,-2*NOTCH_SIZE],
          [HSZ,-2*NOTCH_SIZE],
          [HSZ,0],
          [0,0]
        ])
      }else if(type=='join'){
        return createShape([
          [0,NOTCH_SIZE], //[3*NOTCH_SIZE,NOTCH_SIZE],
          [-HSZ+NOTCH_SIZE,3*NOTCH_SIZE], 
          [-HSZ+NOTCH_SIZE,4*NOTCH_SIZE],
          [-NOTCH_SIZE,4*NOTCH_SIZE],
          [HSZ-NOTCH_SIZE,NOTCH_SIZE],
          [HSZ-NOTCH_SIZE,-NOTCH_SIZE],
          [-NOTCH_SIZE,-4*NOTCH_SIZE],
          [-HSZ+NOTCH_SIZE,-4*NOTCH_SIZE],
          [-HSZ+NOTCH_SIZE,-3*NOTCH_SIZE],
          [0,-NOTCH_SIZE]
        ])
      }else if(type=='table'){
        return createShape([
          [-HSZ+10,HN],//-40,5
          [HSZ-NOTCH_SIZE,HN],
          [HSZ-NOTCH_SIZE,-HN],

          [HN,-HN],
          [HN,-2*NOTCH_SIZE],

          [2*NOTCH_SIZE,-2*NOTCH_SIZE],
          [2*NOTCH_SIZE,-HSZ+NOTCH_SIZE],
          [-2*NOTCH_SIZE,-HSZ+NOTCH_SIZE],
          [-2*NOTCH_SIZE,-2*NOTCH_SIZE],

          [-HN,-2*NOTCH_SIZE],
          [-HN,-HN],
          [-40,-HN]
        ])
      }else {
      return null
    }
  }

  function addNode(context){
    let {scene, type,location}= context

    var material = new THREE.MeshStandardMaterial( { metalness: 0.5, color: 0xaaaaaa, wireframe: false, opacity: 1, transparent: true } );
    var material2 = new THREE.MeshStandardMaterial( { metalness: 0.5, color: 0x444444, wireframe: false, opacity: 1, transparent: true  } );

    let shape = getShape(type)
    if(shape==null) return

    var materials = [ material, material2 ];

    var extrudeSettings = {
      depth: 10,
      steps: 1,
      bevelEnabled: true,
      bevelThickness: 2,
      bevelSize: 3,
      bevelSegments: 8
    };
    var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
    geometry.translate(location.x*100,-location.y*100,15)

    var mesh = new THREE.Mesh( geometry, materials );
    mesh.receiveShadow = true;
    mesh.castShadow = true;

    scene.add( mesh );
  }

  function addConnector(context){
      let {scene, location, connections, merge = false}= context

      let top = connections.reduce((c,hist)=> c > hist ?c : hist , 0)
      let bottom = connections.reduce((c,hist)=> c < hist ? c : hist ,0)

      var material = new THREE.MeshStandardMaterial( { metalness: 0.5, color: 0xaaFFaa, wireframe: false, opacity: 1, transparent: true } );
      var material2 = new THREE.MeshStandardMaterial( { metalness: 0.5, color: 0x44FF44, wireframe: false, opacity: 1, transparent: true  } );
      var materials = [ material, material2 ];
      var extrudeSettings = {
        depth: 10,
        steps: 1,
        bevelEnabled: false,
        bevelThickness: 0,
        bevelSize: 0,
        bevelSegments: 0
      };

      let shape = null
      { 
          shape =  createShape([
            [HSZ-20,HN],//30,5
            [HSZ-HN,HN],//45,5
            [HSZ-HN,-HN],//45,-5
            [HSZ-20,-HN] //30,-5
          ])      
        var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
        geometry.translate(location.x*100,-location.y*100,15)
        var mesh = new THREE.Mesh( geometry, materials );
        //mesh.receiveShadow = true;
        //mesh.castShadow = true;
        scene.add( mesh );
      }
      {      
        shape =  createShape([
          [HSZ-HN,top*NODE_SIZE+HN],
          [HSZ+HN,top*NODE_SIZE+HN],
          [HSZ+HN,bottom*NODE_SIZE-HN],
          [HSZ-HN,bottom*NODE_SIZE-HN]
        ])

        var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
        geometry.translate(location.x*100,-location.y*100,15)
        var mesh = new THREE.Mesh( geometry, materials );
        //mesh.receiveShadow = true;
        //mesh.castShadow = true;
        scene.add( mesh );
      }
      for(let i=0;i<connections.length;i++)
      {
        let pos = connections[i]
        var pts = []; 

        shape =  createShape([
          [HSZ+HN,pos*NODE_SIZE+HN],
          [HSZ+3*HN,pos*NODE_SIZE+HN],
          [HSZ+3*HN,pos*NODE_SIZE-HN],
          [HSZ+1*HN,pos*NODE_SIZE-HN]
        ])  
            
        var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
        geometry.translate(location.x*100,-(location.y*100),15)
        var mesh = new THREE.Mesh( geometry, materials );
        //mesh.receiveShadow = true;
        //mesh.castShadow = true;
        scene.add( mesh );
      }
  }

  let setupScene = (context)=>{
    
    let {container,imageWidth,imageHeight} = context

    while (container.firstChild) {
      container.firstChild.remove();
    }

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize( imageWidth, imageHeight );
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    container.appendChild( renderer.domElement );
 
    let scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xFFFFFF );

    
    
    scene.add( new THREE.AmbientLight( 0xaaaaaa ) );

    let pointlight = new THREE.PointLight( 0xffffff, 0.5 /*Light Intensity*/ );
    pointlight.position.set(20, 20, 200 ); 			//default; light shining from top
    pointlight.castShadow = true;            // default false
    scene.add( pointlight );
    
    function getConnections(n1,n2){
      let d = n1.y-n2.y
      if(n2.type==='merge' || n2.type==='join'){
        return [d+0.35,d-0.35]
      }else {
        return [d]
      }
    }
    let nh = n=>{
      addNode({scene,
               type:n.type,
               location:{x:n.x,y:n.y}
              })  
    }
    let lh = (n1,n2)=>{
      let connections = getConnections(n1,n2)
      addConnector({scene,location: {x:n1.x,y:n1.y}, connections})  
    }
    let topology = Topology({node: nh,link:lh})
    topology.traverseModel(model)

    let b = topology.bounds()
    sceneMax = b[0]> b[1] ? b[0] : b[1]
    if(!sceneMax) sceneMax =1
    
    
      // var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.5 );
      // scene.add( ambientLight );

    var groundGeometry = new THREE.PlaneBufferGeometry( sceneMax*500, 400, 1, 1 );
    groundGeometry.translate(100,0,0)
    var groundMaterial = new THREE.MeshPhongMaterial( { metalness: 1, color: 0x888888 } );
    var ground = new THREE.Mesh( groundGeometry, groundMaterial );
    //ground.rotation.x = Math.PI * - 0.5;
    ground.receiveShadow = true; //default
    scene.add( ground );

    //let camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
    let camera = new THREE.PerspectiveCamera( 45, imageWidth / imageHeight, 1, 2000 );
    camera.position.set( 30, 30, sceneMax*200 );

    return {
      pointlight,
      camera,
      renderer,
      scene
    }

  }

  let createController = (entities)=>{

    let { pointlight } = entities
    
    let dx = 1
    let dy =1

    return {
        move: function() {
          if(pointlight.position.x>20 && pointlight.position.y>20) {
            dx=-1
            dy=0
          }
          if(pointlight.position.x<-20 && pointlight.position.y<-20) {
            dx=1
            dy=0
          }
          if(pointlight.position.x<-20 && pointlight.position.y>20) {
            dx=0
            dy=-1
          }
          if(pointlight.position.x>20 && pointlight.position.y<-20) {
            dx=0
            dy=1
          }
          pointlight.position.x = pointlight.position.x+dx
          pointlight.position.y = pointlight.position.y+dy
        }
        
    }
  }

  let setup = ()=>{

    let { 
      width: imageWidth =window.innerWidth,
      height: imageHeight =window.innerHeight,
    } = props

    const container = document.getElementById(sceneId);


     let {scene, camera , renderer, pointlight } =setupScene({controller,container, imageWidth,imageHeight})
    
      let controller = createController({pointlight})

      let controls = new TrackballControls( camera, renderer.domElement );
      controls.minDistance = 300;
      controls.maxDistance = sceneMax*300;
  
      function animate() {
        requestAnimationFrame( animate );
        render()
      }

      function render() {
        controls.update();
        renderer.render(scene, camera);
        controller.move()
      }
      animate();
    }

    useEffect(setup)

    let desc = ''
    if(showDescription){
      desc = <p>{node.description}<br/>{node.rationale}</p>
    }
    return <div>
      {desc}
      <div id={sceneId}>
      
     </div>
    </div>
    
  }

export default SceneComponent

