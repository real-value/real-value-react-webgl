import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';

import { Model as modelFactory } from 'real-value-lang'

const EvaluateComponent=  ({className,expression,multiresults=false,content,logic='',enabled=true}) => {


  const [exp,setExp]= useState(expression)
  const [result, setResult] = useState('');
  const [result2, setResult2] = useState('');

  let variables = {}

  const model = modelFactory({variables})
  
  let res=''
  let res2=''

  const disabled = !enabled

  const capture1 = x=>{
    res =  `${res}|${typeof x==='object'?JSON.stringify(x):x}`
    setResult(res)
  }
  const capture2 = x=>{
    res2 =  `${res2}|${typeof x==='object'?JSON.stringify(x):x}`
    setResult2(res2)
  }

  function updateExpression(v){
    setExp(v.target.value)
  }

  const evaluate = (event) => {
    try {
      let n = eval(exp)
      if(!multiresults){
        n.tap(capture1)
      }
      model.run(()=>{
        //console.log('model done')
      })
    }catch(ex){
      console.log(ex)
      setResult(ex)
    }
  }

  return <React.Fragment>
        <Grid item xs={4}>{content}<pre>{logic}</pre></Grid>
        <Grid item xs={5}><textarea disabled={disabled} type="text" cols="80" rows="3" value={exp} onChange={updateExpression}></textarea></Grid>
        <Grid item xs={1}><button disabled={disabled} onClick={evaluate}>Eval</button></Grid>
        <Grid item xs={1}><pre>{result}</pre><pre>{result2}</pre></Grid>  
        </React.Fragment>
}

export default EvaluateComponent

